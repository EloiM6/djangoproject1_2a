from django.shortcuts import render

# Create your views here.
from django.http import HttpRequest, HttpResponse
from DjangoRambo.models import Persona,Adressa

# Create your views here.
from django.views.generic.base import View
class DjangoRambo(View):
    # Definim el mètode HTTP el qual s'ha d'atendre
    def get(self,data ):
        return HttpResponse(content='Això és una prova')

class DjangoRambo2(View):
    def get(self,request):
        Adresanova = Adressa.objects.create(Nom="Adressa1", TipusVia=1)
        Per1 = Persona.objects.create(Nom="Alex", Cognoms="Prova11", DNI="12345678J ", Genere = 1, Adressa = Adresanova)
        Per2 = Persona.objects.last()
        return HttpResponse(content='El nom de la persona és ' + str(Per2.Nom))
