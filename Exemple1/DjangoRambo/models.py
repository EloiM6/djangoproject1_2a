from django.db import models

# Create your models here.

Genere = ((0, 'Dona'), (1, 'Home'), (2, 'Altres'), (3, 'No binari'))
TipusVia = ((0, 'Carrer'), (1, 'Carretera'), (2, 'Avinguda'), (3, 'Cami'))


class Adressa(models.Model):
    Nom = models.CharField(max_length=30, null=False, default="")
    TipusVia = models.CharField(max_length=30, choices=TipusVia)


class Persona(models.Model):
    Nom = models.CharField(max_length=30, null=False, default="")
    Cognoms = models.CharField(max_length=50, null=False, default="")
    DNI = models.CharField(max_length=9, unique=True)
    Genere = models.CharField(choices=Genere, max_length=30)
    Adressa = models.ForeignKey(Adressa, on_delete=models.CASCADE)
